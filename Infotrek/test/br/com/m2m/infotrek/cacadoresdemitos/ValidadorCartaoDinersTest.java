package br.com.m2m.infotrek.cacadoresdemitos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ValidadorCartaoDinersTest {
	
	private Validador validador;
	
	@Before
	public void setUp(){
		validador = new ValidadorCartaoDiners();
	}
	
		
	@Test public void
	naoDeveIdentificarCartaoDiners() {
		assertFalse(validador.valido("20000000000004"));
	}
	
	@Test public void
	deveIdentificarCartaoDinersComDigitosIniciais36(){
		assertTrue(validador.valido("36438936438936"));
	}
	
	@Test public void
	deveIdentificarCartaoDinersComDigitosIniciais38(){
		assertTrue(validador.valido("38520000023237"));
	}
	
	@Test public void
	deveIdentificarCartaoDinersComDigitosIniciais301(){
		assertTrue(validador.valido("30146656445116"));
	}
	
	@Test public void
	deveIdentificarCartaoDinersComDigitosIniciais302(){
		assertTrue(validador.valido("30204169322643"));
	}
	
	@Test public void
	deveIdentificarCartaoDinersComDigitosIniciais303(){
		assertTrue(validador.valido("30358712765870"));
	}
	
	@Test public void
	deveIdentificarCartaoDinersComDigitosIniciais304(){
		assertTrue(validador.valido("30041435880727"));
	}
	
	@Test public void
	deveIdentificarCartaoDinersComDigitosIniciais305(){
		assertTrue(validador.valido("30569309025904"));
	}
	
	@Test public void
	naodeveIdentificarCartaoDinersComDigitosIniciais306(){
		assertFalse(validador.valido("30600000000004"));
	}
	
	@Test public void
	naoDeveValidarCartaoDinersComTamanhoIncompativel(){
		assertFalse(validador.valido("3643893643893"));
	}
	
	
}
