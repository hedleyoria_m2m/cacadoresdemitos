package br.com.m2m.infotrek.cacadoresdemitos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ValidadorCartaoVisaTest {
	
	private Validador validador;
	
	@Before
	public void setUp(){
		validador = new ValidadorCartaoVisa();
	}
	
	@Test public void
	deveIdentificarCartaoVisaCom16Digitos(){
		assertTrue(validador.valido("4111111111111111"));
	}
	
	@Test public void
	deveIdentificarCartaoVisaCom13Digitos(){
		assertTrue(validador.valido("4222222222222"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoVisaComDigitosMenorQue13() {
		assertFalse(validador.valido("411111111111"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoVisaComDigitosMaiorQue16() {
		assertFalse(validador.valido("41111111111111111"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoQueNaoComeceCom4() {
		assertFalse(validador.valido("51111111111111111"));
	}
	
}
