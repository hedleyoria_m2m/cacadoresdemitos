package br.com.m2m.infotrek.cacadoresdemitos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ValidadorCartaoAmericanExpressTest {
	
	private Validador validador = null;
	
	@Before
	public void setUp(){
		validador = new ValidadorCartaoAmericanExpress();
	}
	
	@Test public void
	deveValidarCartaoDeBandeiraAmex() throws Exception{
		assertTrue(validador.valido("378282246310005"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoAmex() {
		assertFalse(validador.valido("478282246310005"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoAmexComTamanhoIncompativel() {
		assertFalse(validador.valido("37828224631005"));
	}
	
	
	@Test public void
	deveIdentificarCartaoAmexComDigitosIniciais34(){
		assertTrue(validador.valido("343434343434343"));
	}

}
