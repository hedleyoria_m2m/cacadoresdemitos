package br.com.m2m.infotrek.cacadoresdemitos;

import br.com.m2m.infotrek.cacadoresdemitos.util.CartaoUtil;

public class ValidadorCartaoVisa implements Validador {

	@Override
	public boolean valido(String numero) {
		int cardID;
		if ((cardID = identificarBandeira(numero)) != -1)
			return new CartaoUtil().LUHMMod10(numero);
		return false;
	}

	private int identificarBandeira(String numero) {
		int codigo = CartaoUtil.INVALIDO;
		String digit1 = numero.substring(0, 1);
		
		/*
		 * ----* VISA prefix=4* ---- length=13 or 16 (será que pode ser 15!?! talvez... :( )
		 */
		if (digit1.equals("4")) {
			if (numero.length() == 13 || numero.length() == 16)
				codigo = CartaoUtil.VISA;
		}
		return codigo;
	}

}
