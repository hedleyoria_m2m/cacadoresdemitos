package br.com.m2m.infotrek.cacadoresdemitos.util;

public class CartaoUtil {
	
	public static final int INVALIDO = -1;
	public static final int VISA = 0;
	public static final int MASTERCARD = 1;
	public static final int AMERICAN_EXPRESS = 2;
	public static final int EN_ROUTE = 3;
	public static final int DINERS_CLUB = 4;

	public static final String[] bandeiras = { "Visa", "Mastercard",
			"American Express", "En Route", "Diner's Club", };

	public boolean LUHMMod10(String numero) {
		try {
			/**
			 * Formula LUHN (mod10)
			 */
			int j = numero.length();

			String[] s1 = new String[j];
			for (int i = 0; i < numero.length(); i++)
				s1[i] = "" + numero.charAt(i);

			int checksum = 0;

			for (int i = s1.length - 1; i >= 0; i -= 2) {
				int k = 0;

				if (i > 0) {
					k = Integer.valueOf(s1[i - 1]).intValue() * 2;
					if (k > 9) {
						String s = "" + k;
						k = Integer.valueOf(s.substring(0, 1)).intValue()
								+ Integer.valueOf(s.substring(1)).intValue();
					}
					checksum += Integer.valueOf(s1[i]).intValue() + k;
				} else
					checksum += Integer.valueOf(s1[0]).intValue();
			}
			return ((checksum % 10) == 0);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
