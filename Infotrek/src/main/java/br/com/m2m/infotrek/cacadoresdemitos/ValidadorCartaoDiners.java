package br.com.m2m.infotrek.cacadoresdemitos;

import br.com.m2m.infotrek.cacadoresdemitos.util.CartaoUtil;

public class ValidadorCartaoDiners implements Validador {

	@Override
	public boolean valido(String numero) {
		int cardID;
		if ((cardID = identificarBandeira(numero)) != -1)
			return new CartaoUtil().LUHMMod10(numero);
		return false;
	}

	public int identificarBandeira(String numero) {
		int codigo = CartaoUtil.INVALIDO;

		String digit2 = numero.substring(0, 2);
		String digit3 = numero.substring(0, 3);
		
		/*
		 * -----* DCLUB prefix=300 ... 305 or 36 or 38* ----- length=14
		 */
		if (digit2.equals("36")
				|| digit2.equals("38")
				|| (digit3.compareTo("300") >= 0 && digit3.compareTo("305") <= 0)) {
			if (numero.length() == 14)
				codigo = CartaoUtil.DINERS_CLUB;
		}
		return codigo;
	}

}
